﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(QLiSP.Startup))]
namespace QLiSP
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
